<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login To Dilla Catering</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="Sign in/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Sign in/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Sign in/css/util.css">
	<link rel="stylesheet" type="text/css" href="Sign in/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-b-160 p-t-50">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-43">
						SIGN IN TO DILLA CATERING
					</span>
					
					<div class="wrap-input100 rs1 validate-input" data-validate = "Username is required">
						<input class="input100" type="text" name="username">
						<span class="label-input100">Username</span>
					</div>
					
					
					<div class="wrap-input100 rs2 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="pass">
						<span class="label-input100">Password</span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Sign in
						</button>
					</div>
					
					<div class="text-left w-full p-t-23">
						<a href="#" class="txt1">
							Forgot password?
						</a>
					</div>
                    <div class="text-left w-full p-t-23">
						<a href="/register" class="txt1">
							Sign UP
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
	<script src="Sign in/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/vendor/bootstrap/js/popper.js"></script>
	<script src="Sign in/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/vendor/daterangepicker/moment.min.js"></script>
	<script src="Sign in/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="Sign in/js/main.js"></script>

</body>
</html>